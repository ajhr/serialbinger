<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Serial Bingers</title>
    <link rel="stylesheet" href="../stylesheets/style.css">
  </head>
  <body>
    <header>
      <div>
        <a href="../index.php">back</a>
      </div>
      <h1>Serial Bingers</h1>
    </header>
    <div id="content">
      <h2>Watchers</h2>
      
    <?php
    	include 'db_connection.php';

    	// 1. Create a database connection
    	$connection = openDbConnection();

    	// 2. Perform database query
    	$query = "SELECT * FROM Watcher";
    	$result_set = mysqli_query($connection, $query);

    	// Test if query succeeded
    	if (!$result_set) {
    		exit("Database query failed.");
    	}

    	// 3. Use returned data (if any)
    	echo "<table class='list'>";
    	echo "<tr><th>Watcher name</th></tr>";
    	while($watcher = mysqli_fetch_assoc($result_set)) {
      		echo "<tr><td><a href='../series.php?id=" . $watcher["WatcherID"] . "'>" . $watcher["WatcherName"] . "</a></td></tr>";
    	}
    	echo "</table>";

    	// 4. Release returned data
    	mysqli_free_result($result_set);

    	// 5. Close database connection
    	closeDbConnection($connection);
    ?>
  </div>
  </body>
</html>
