<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Serial Binger</title>
    <link rel="stylesheet" href="stylesheets/style.css">
  </head>
  <body>
    <header>
      <div>
        <a href="../watchers.php">back</a>
      </div>
      <h1>Watcher's digest</h1>
    </header>
    <div id="content">
      <h2>Series</h2>
      
	<?php
    	include 'db_connection.php';

    	// 1. Create a database connection
    	$connection = openDbConnection();

    	$watcher_id = $_GET['id'] ?? '1';

    	// 2. Perform database query
    	$query = "SELECT * FROM Watcher WHERE WatcherID = '" . $watcher_id . "'";
    	$result = mysqli_query($connection, $query);

    	// Test if query succeeded
    	if (!$result) {
    		exit("Database query failed.");
    	}

    	// 3. Use returned data (if any)
    	$watcher = mysqli_fetch_assoc($result);
    	echo "<p>Watcher: " . $watcher["WatcherName"] . "</p>";

    	$query = "SELECT * FROM Series WHERE WatcherID = '" . $watcher_id . "';";
    	$result_set = mysqli_query($connection, $query);

    	// Test if query succeeded
    	if (!$result_set) {
      		exit("Database query failed.");
    	}
    	if (mysqli_num_rows($result_set) == 0) {
      		echo "(No series found)";
    	}
    	else {
      		echo "<table class='list'>";
      		echo "<tr><th>Series Name</th><th>Start date</th><th>Watcher rating</th><th>Comment</th></tr>";
      		while($series = mysqli_fetch_assoc($result_set)) {
        		echo "<tr><td>" . $series["SeriesName"] . "</td><td>" . $series["StartDate"] . "</td><td>" . $series["WatcherRating"] . "</td><td>" . $series["Comment"] . "</td></tr>";
      		}
      		echo "</table>";
    	}

    	// 4. Release returned data
    	mysqli_free_result($result);
    	mysqli_free_result($result_set);

    	// 5. Close database connection
    	closeDbConnection($connection);

    ?>
  </div>
  </body>
</html>
