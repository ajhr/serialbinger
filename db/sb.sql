-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:80
-- Generation Time: Oct 05, 2019 at 11:53 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

-- --------------------------------------------------------------------

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `SerialBinger` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `SerialBinger`;

-- --------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Series` (
  `SeriesID` int(11) NOT NULL,
  `SeriesName` varchar(50) NOT NULL,
  `WatcherID` int(5) NOT NULL,
  `StartDate` date NOT NULL,
  `WatcherRating` int(2) NOT NULL,
  `Comment` varchar(500),
  PRIMARY KEY (`SeriesID`),
  KEY `Series_Watcher` (`WatcherID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Watcher` (
  `WatcherID` int(5) NOT NULL,
  `WatcherName` varchar(15) NOT NULL,
  PRIMARY KEY (`WatcherID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------------------

ALTER TABLE `Series`
  ADD CONSTRAINT `Series_Watcher` FOREIGN KEY (`WatcherID`) REFERENCES `Watcher` (`WatcherID`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------------------

INSERT INTO Watcher (WatcherID, WatcherName)
VALUES (1, 'Zulema Zahir');

INSERT INTO Watcher (WatcherID, WatcherName)
VALUES (2, 'James Ford');

INSERT INTO Watcher (WatcherID, WatcherName)
VALUES (3, 'Michael Burnham');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (1, 'Vis a Vis', 1, '2019-09-01', 0, 'Hate it.');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (2, 'Vis a Vis', 2, '2019-09-07', 10, 'So many woman, just cannot choose. But why choose?');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (3, 'Vis a Vis', 3, '2019-09-05', 9, 'No tardigrades but still awesome.');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (4, 'Lost', 1, '2004-12-27', 8, 'Quite OK, but not enough escaping.');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (5, 'Lost', 2, '2004-12-11', 10, 'What a great actor, that Sawyer guy! And so handsome!');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (6, 'Lost', 3, '2005-01-17', 5, 'Such old tech.');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (7, 'Discovery', 2, '2018-03-13', 10, 'Exsaruordaniry!');

INSERT INTO Series (SeriesID, SeriesName, WatcherID, StartDate, WatcherRating, Comment)
VALUES (8, 'Discovery', 3, '2018-03-13', 9, 'Might be the best one of the franchise.');

-- --------------------------------------------------------------------

GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost' IDENTIFIED BY 'user!';

-- --------------------------------------------------------------------